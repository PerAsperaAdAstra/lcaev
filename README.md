# Low-Cost Automated Emergency Ventilator v0.2

This project is intended to provide a low-cost, easy to produce, easy to use, automated
ventilator FOR EMERGENCY USE ONLY WHEN NO MEDICAL DEVICES ARE AVAILABLE.  This is to be
used ONLY as an alternative to death, not to certified medical equipment.  This code
and instruction set is released as-is with the user assuming all risks and agreeing
to hold all contributors harmless.  This is a desperate measure for a desperate time.

Copyright Sam Pedrotty 2020.  This source describes Open Hardware and is licensed under the
CERN-OHL-P v2.

You may redistribute and modify this documentation and make products using it under
the terms of the CERN-OHL-P v2 (https:/cern.ch/cern-ohl).  This documentation is 
distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, 
SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-P v2
for applicable conditions.

# [For The Latest Information, Please See The Wiki](https://gitlab.com/PerAsperaAdAstra/lcaev/-/wikis/home)

https://gitlab.com/PerAsperaAdAstra/lcaev/-/wikis/home

## Assembly

Download part files from the wiki.

### Non-Printed Components
* 1x Raspberry Pi 4 (any version)
    * https://www.amazon.com/Raspberry-Model-2019-Quad-Bluetooth/dp/B07TD42S27/
* 1x microSD card (recommend >8 GB)
    * https://www.amazon.com/gp/product/B073JWXGNT/
* 2x NEMA 17 stepper motors (recommend >= 0.4 N-m torque)
    * https://www.amazon.com/4-Piece-17HS4401-Torque-Bipolar-Stepper/dp/B07TZTLJMT/
* 1x Uctronics 7" touch screen
    * https://www.amazon.com/UCTRONICS-Raspberry-1024%C3%97600-Capacitive-Touchscreen/dp/B07VWDDWQ9/
* 1x small USB speaker
    * https://www.amazon.com/HONKYOB-Speaker-Computer-Multimedia-Notebook/dp/B07K7JY7Q7/
* 1x Adafruit Pi Stepper Hat
    * https://www.amazon.com/Adafruit-Stepper-Motor-HAT-Raspberry/dp/B00TIY5JM8/
* 1x 5v 2a+, dual USB output wall wart
    * https://www.amazon.com/gp/product/B07437M5QQ/
* 1x 5v 4a wall wart
    * https://www.amazon.com/gp/product/B01N4HYWAM
 * 1x Ambubag, PEEP valve, viral filter
    * (expected to be provided by healthcare facility)
* 7x Zip ties
* 8x M3x8 screws
* Loctite
* Optional:
    * 1x GY-86 barometer breakout
        * https://www.amazon.com/LICTOP-Precision-Barometric-Pressure-Temperature/dp/B07NMXD3J9/


### Build Instructions

1. Order parts
1. Print components
    * 1x: LCAEV-0.2_bag_connector (print at 25% infill PLA, 3 walls)
    * 1x: LCAEV-0.2_base_connector (print at 25% infill PLA, 3 walls)
    * 2x: LCAEV-0.2_base_half (print at 25% infill PLA, 3 walls)
    * 1x: LCAEV-0.2_electronics_housing (print at 25% infill PLA, 3 walls)
    * 2x: LCAEV-0.2_piston (print at 25% infill PLA, 3 walls)
    * 1x: LCAEV-0.2_screen_support (print at 25% infill PLA, 3 walls)
    * 2x: LCAEV-0.2_spur_gear (print at 100% infill PETG, 3 walls)
    * Optional:
        * 1x: LCAEV-0.2_handle (print at 25% infill PLA, 3 walls)
        * 1x: LCAEV-0.2_sensor_housing (print at 25% infill PLA, 3 walls)
1. Set up computer
    * write latest raspbian to sd card using the Raspberry Pi 'Imager' application
    * insert card into pi and boot (connect to power and peripherals)
    * walk through the pi setup prompts that appear at the first boot (be sure to connect to the internet)
    * clone this repository
        * git clone https://gitlab.com/PerAsperaAdAstra/lcaev.git
    * run the setup/install.py file and follow its instructions
        * cd lcaev/setup
        * sudo python3 install.py
    * restart the pi to verify the GUI comes up as expected
1. Gently hammer the spur gears onto the stepper motor spindles
1. Align and mate the bases (glue optional)
1. Align the base connector with the half-ring cutouts and attach with zip ties
1. Align the bag connector on the opposite side's half-ring cutouts and attach with zip ties
1. Install the stepper motors (Loctite and M3 screws)
1. Slide the pistons into place
1. Assemble the pi hat
1. Install onto pi and slide into electronics enclosure
1. Slide screen into electronics housing and connect screen USB and HDMI to pi
1. Connect speaker USB to pi
1. Slide speaker into receptacle and zip tie in place
1. Connect power leads and stepper leads to HAT
1. Connect stepper power/data cables from stepper to HAT leads
1. Set the ambubag into the assembly and zip tie its outlet to the bag connector arms
1. (optional)
    * insert BMP180 barometer into housing and tack with glue.  Fill in holes around pins with glue (don't put so much that you can't connect to the pins!).  Attach sensor wires to pi GPIO and sensor.
    * zip tie the handle onto the frame
1. Connect to power, boot, and verify function


## Use

**NOTE: this procedure has not been tested and may be seriously flawed**

1. set up LCAEV near patient
1. zip tie ambubag into system
2. connect both LCAEV wall warts to power (LCAEV then should automatically boot)
3. wait for GUI to come up
4. connect ambubag to patient circuit
    * DO NOT CONNECT THE CIRCUIT TO THE PATIENT
    * NOTE: patient circuit must contain valving near the mouth to minimize dead volume in the circuit
4. connect viral filter to exhalation side of the circuit (provided by the user)
1. connect PEEP valve after viral filter (provided by the user)
    * DO NOT CONNECT THE CIRCUIT TO THE PATIENT
1. Follow the GUI prompts to begin ventilation
    * calibrate barometer
    * verify system working during self-test
    * set respiratory rate, tidal volume, start ventilation
    * update as needed, enable/disable assist control, mute alarm
    * if any issues are observed, restart the unit by unplugging it from the wall and plugging it back in

## Software License
Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
