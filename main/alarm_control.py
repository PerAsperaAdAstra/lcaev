#!/usr/bin/python3

'''
alarm_control.py

This program is intended to control 
alarms

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import sys
import os
import time
#import pygame
import json
import psutil


###########################
#SUPPORT FUNCTIONS
###########################


def alarm_operate():

    print('starting main alarm function')

    #set vars
    start_time = time.time()
    volume = -7
    time_interval = 1
    mode = 'IDLE'
    mute = 0
    old_mute = 0
    baro_alarm = False
    baro_status = True
    baro_mute = False
    patient_disconnect = False
    patient_disconnect_alarm = False
    patient_disconnect_mute = False

    sound_filename = 'alarm_sound.wav'
    file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))


    while True:


        if mode == 'CHECKOUT':
            print('running alarm checkout...')
            #pygame.mixer.music.play()

            os.system('mplayer -nolirc ' + os.path.join(file_dir, sound_filename) + '  -af volume='+str(volume))
            mode = 'IDLE'
            time.sleep(5) #should eventually be 1


        if mode == 'OPERATE':

            if mute > old_mute:
                baro_mute = True
                patient_disconnect_mute = True

                old_mute = mute

            if baro_status == False: baro_alarm = True
            else: baro_alarm = False

            if patient_disconnect == True: patient_disconnect_alarm = True
            else: patient_disconnect_alarm = False

            if baro_mute == True: baro_alarm = False
            if patient_disconnect_mute == True: patient_disconnect_alarm = False


            if baro_alarm == True:
                print('BARO FAIL ALARM')
                os.system('mplayer -nolirc ' + os.path.join(file_dir, sound_filename) + '  -af volume='+str(volume)+' > /dev/null 2>&1')

            elif patient_disconnect_alarm == True:
                print('PATIENT DISCONNECT ALARM')
                os.system('mplayer -nolirc ' + os.path.join(file_dir, sound_filename) + '  -af volume='+str(volume)+' > /dev/null 2>&1')


            #list running python processes
            proc_list = []
            for proc in psutil.process_iter():
                if proc.name() == 'python3':
                    if len(proc.cmdline()) > 1:
                        script_path = ' '.join(proc.cmdline())
                        if 'stepper_control.py' in script_path:
                            proc_list += ['stepper']
                        if 'controller_gui.py' in script_path:
                            proc_list += ['gui']

            #print(proc_list)
            if 'gui' not in proc_list:
                print('GUI CRASH ALARM')
                os.system('mplayer -nolirc ' + os.path.join(file_dir, sound_filename) + '  -af volume='+str(volume)+' > /dev/null 2>&1')

            if 'stepper' not in proc_list:
                print('STEPPER CONTROL CRASH ALARM')
                os.system('mplayer -nolirc ' + os.path.join(file_dir, sound_filename) + '  -af volume='+str(volume)+' > /dev/null 2>&1')



        #check for new inputs
        if time.time()-start_time > time_interval:

            try:
                with open(os.path.join(file_dir,'commands_to_alarm.json')) as f:
                    data = json.load(f)
                mode = data['mode']
                mute = int(data['mute'])

            except: pass

            try:
                with open(os.path.join(file_dir,'baro_status.json')) as f:
                    data = json.load(f)
                baro_status = data['baro_status']
                patient_disconnect = data['patient_disconnect']

                #print(baro_status)
                #mute = int(data['mute'])

            except: pass

            start_time = time.time()

        #update alarm parameters
        if baro_status == True:
            baro_mute = False

        if patient_disconnect == False:
            patient_mute = False



        time.sleep(1) #should eventually be 1


def beep():

    #import pygame
    #pygame.mixer.init()
    #pygame.mixer.music.load('alarm_short.wav')
    #print('starting demo alarm code')
    #pygame.mixer.music.play()
    volume = -7

    print('beep')
    sound_filename = 'alarm_sound.wav'
    file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    os.system('mplayer -nolirc ' + os.path.join(file_dir, sound_filename) + '  -af volume='+str(volume))




###########################
#MAIN CODE
###########################

#pygame.mixer.init()
#pygame.mixer.music.load('alarm_short.wav')

if __name__ == '__main__':

    
    try: args = sys.argv[1]
    except: args = None

    if args == 'MAIN': alarm_operate()
    else: beep()

    




