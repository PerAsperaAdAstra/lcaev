#!/usr/bin/python3

'''
baro_control.py

This program is intended to control 
a BMP180 barometer

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import os
import csv
import sys
import json
import time
import socket
import numpy as np
from datetime import datetime
import Adafruit_BMP.BMP085 as BMP085



###########################
#SUPPORT FUNCTIONS
###########################
def log_baro_data(socket_flag = True, mode = None):

    #init vars
    baro_status = False
    VERBOSE = False
    start_time = time.time()
    file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    time_interval = 1 #seconds between settings changes

    the_start = time.time() #time since power on bias
    data_start_time = time.time() #variable for data logging time tracking 
    data_time_interval = 30 #seconds between settings changes
    log_data = {'ontime':[],'time':[],'pressure':[],'temperature':[],'status':[],'delta_pressure':[]}
    cal_complete = False
    std_pressure = 0
    last_mode = 'IDLE'
    last_baro_status = True
    patient_disconnect = False
    Pabs_delta_dc_window = [999,999,999,999,999,999,999,999,999,999,999,999]
    last_pressure_read = 0
    if mode == None: mode = 'IDLE'
    run_rate = 10 #Hz

    #create sensor objects
    try:
        baro = BMP085.BMP085(mode=BMP085.BMP085_ULTRAHIGHRES)
        print('BARO RECONNECTED!')
    except:
        print('FAILED TO CONNECT TO BARO')
        baro = False

    if socket_flag:
        #socket stuff
        host = ''        # Symbolic name meaning all available interfaces
        port = 12345     # Arbitrary non-privileged port
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
        #s.setblocking(0)
        s.bind((host, port))
        s.listen(1)
        print('socket configured in baro!')

        print('trying to connect...')
        conn, addr = s.accept()
        print('baro connected to client in baro!')


    #do stuff 4ever
    while True:              

        #slow down
        time.sleep(1.0/run_rate)


        if mode == 'CALIBRATE':

            if baro == False:
                print('\nERROR: BARO NOT FOUND\n')
                cal_complete = 'NO BARO'
            else:
                meas_list = [0] #must be nonzero-length list in case baro fails
                num_samples = 50
                baro_status = True
                for q in range(0,num_samples):
                    try:
                        meas_list += [baro.read_pressure()]
                        print(baro.read_pressure())

                    except:
                        print('\nERROR: BARO NOT FOUND\n')
                        cal_complete = 'NO BARO'
                        baro_status = False
                    time.sleep(0.1)

                if len(meas_list) > 1: meas_list.pop(0) #ditch the fake 0 before we get into math
                std_pressure = sum(meas_list)/num_samples
                max_delta = max(meas_list)-min(meas_list)

                if max_delta > 1000:
                    cal_complete = 'FAILURE'
                    baro_status = False
                    print('\nBARO OUTLIERS DETECTED\n')
                    print(max(meas_list))
                    print(min(meas_list))
                print(sum(meas_list))
                if baro_status == True: cal_complete = True


                print('\ncal complete!\n')


        if mode == 'OPERATE':

            #read baro
            try:
                #PST = time.time()
                log_data['delta_pressure'] += [baro.read_pressure() - std_pressure]
                #print('baro read took: ' + str(time.time()-PST) + ' s')
                log_data['ontime'] += [time.time()-the_start]
                log_data['time'] += [time.time()]
                log_data['pressure'] += [baro.read_pressure()]
                log_data['temperature'] += [baro.read_temperature()]
                log_data['status'] += ['idle']
                baro_status = True

                #update delta_buffer
                Pabs_delta_dc_window += [log_data['pressure'][-1]-last_pressure_read]
                last_pressure_read = log_data['pressure'][-1]
                Pabs_delta_dc_window.pop(0)
                Pabs_delta_dc_window = [abs(x) for x in Pabs_delta_dc_window]
                if max(Pabs_delta_dc_window) < 50:
                    patient_disconnect = True
                else: patient_disconnect = False

            except:

                baro_status = False
                if baro_status != last_status:
                    print('BARO FAILURE DETECTED!')
                #should then write it to the JSON so that the alarm can sound
                # should also disable assist mode
                log_data['delta_pressure'] += [0]
                log_data['ontime'] += [time.time()-the_start]
                log_data['time'] += [time.time()]
                log_data['pressure'] += [np.nan]
                log_data['temperature'] += [np.nan]
                log_data['status'] += ['ERROR']


            #send data over socket
            if socket_flag:# and baro_status:
                try:
                    conn.sendall(str({'time':log_data['time'][-1],'delta_pressure':log_data['delta_pressure'][-1]}).encode('utf-8'))
                    #print('SENT!')
                except BrokenPipeError:
                    print('trying to connect...')
                    conn, addr = s.accept()
                    print('baro connected to client in baro!')
                except ConnectionResetError:
                    print('trying to connect...')
                    conn, addr = s.accept()
                    print('baro connected to client in baro!')
                except TimeoutError:
                    print('timed out')


        else: #idle
            time.sleep(1)

        last_status = baro_status

        #check for new inputs and write status
        if time.time()-start_time > time_interval:

            try:
                with open(os.path.join(file_dir,'commands_to_baro.json')) as f:
                    data = json.load(f)
                mode = data['mode']

                #only want to cal once
                if last_mode == 'CALIBRATE':
                    mode = 'IDLE'
                    last_mode = mode
                else: last_mode = mode

                #update window size
                window_size = int(run_rate*60/int(data['resp_rate']))
                while len(Pabs_delta_dc_window) != window_size:
                    if len(Pabs_delta_dc_window) < window_size:
                        Pabs_delta_dc_window += [999]
                    if len(Pabs_delta_dc_window) > window_size:
                        Pabs_delta_dc_window.pop(0)

            except: pass

            status_dict = {'baro_status':baro_status, 'cal_complete':cal_complete, 'mode':data['mode'], 'patient_disconnect':patient_disconnect}
            with open(os.path.join(file_dir,'baro_status.json'), 'w') as f:
                json.dump(status_dict, f)

            start_time = time.time()


        # save data less frequently
        if time.time()-data_start_time > data_time_interval:

            now = str(datetime.now())
            now = now.split('.')
            now = now[0]
            now = now.replace(' ','_')
            now = now.replace(':','-')

            #print(log_data)
            keys = sorted(log_data.keys())
            with open(os.path.join(os.path.dirname(file_dir),'analysis','data','TEST_BARO_DATA_'+now+'.csv'),'w') as csv_file:
               writer=csv.writer(csv_file)
               writer.writerow(keys)
               writer.writerows(zip(*[log_data[key] for  key in keys]))

            log_data['temperature'] = []
            log_data['pressure'] = []
            log_data['delta_pressure'] = []
            log_data['ontime'] = []
            log_data['time'] = []
            log_data['status'] = []
            data_start_time = time.time()


###########################
#MAIN CODE
###########################

if __name__ == "__main__":


    try: args = sys.argv[1]
    except: args = None

    if args == '-nosocket': log_baro_data(socket_flag=False)
    elif args == 'MAIN': log_baro_data()
    else: log_baro_data(mode = 'OPERATE')



