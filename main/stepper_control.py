#!/usr/bin/python3

'''
stepper_control.py

This program is intended to control 
stepper motors via the adafruit
stepper shield for Pis

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import os
import ast
import sys
import time
import json
import atexit
import socket
from adafruit_motor import stepper
from datetime import datetime
from adafruit_motorkit import MotorKit




###########################
#SUPPORT FUNCTIONS
###########################

# release/turn off motors at exit
def turnOffMotors():
    kit.stepper1.release()
    kit.stepper2.release()


def extend(kitstepper1,kitstepper2,steps,delay,Baro=None,meas_per_cycle=0):

    delta_delay = delay
    for i in range(steps):
        try:
            kitstepper1.onestep(style = stepper.DOUBLE)
        except: print('\n' + str(round(time.time(),5)) + ': WHOOPS SKIPPED A STEP1e')

        if Baro is not None:
            meas_idx = int(steps/meas_per_cycle)
            if i%meas_idx == 0 and i>0 or i==(steps-1) :
                meas_start = time.time()
                #pull baro data if it's there
                bflag = 0
                try:
                    the_start_time = time.time()
                    data_baro = Baro.recv(10000)
                    bflag = 1
                except:
                    print('SOCKET RECEIVE FAIL')
                    pass
                if bflag == 1:
                    qwe = data_baro.decode('ascii')
                    asd = qwe.split('}')  #catch multiple messages in the same read
                    baro_data = []
                    for item in asd:
                        if len(item) > 0:
                            baro_data += [item+'}']
                    try: data_baro= ast.literal_eval(baro_data[-1])
                    except: 
                        try: data_baro = ast.literal_eval(baro_data[-2])
                        except: print('SOCKET DATA READ FAIL')
                    the_end_time = time.time()

                delta_delay = time.time()-meas_start
                if delta_delay < 0: delta_delay = 0

            else: delta_delay = delay


        time.sleep(delta_delay) #move delays in between to increase spacing between

        try: kitstepper2.onestep(style = stepper.DOUBLE)
        except: print('\n' + str(round(time.time(),5)) + ': WHOOPS SKIPPED A STEP2e')



def retract(kitstepper1,kitstepper2,steps,delay,meas_per_cycle=0):

    for i in range(steps):
        try: kitstepper1.onestep(direction=stepper.BACKWARD, style = stepper.DOUBLE)
        except: print('\n' + str(round(time.time(),5)) + ': WHOOPS SKIPPED A STEP1r')

        time.sleep(delay) #move delays in between to increase spacing between

        try: kitstepper2.onestep(direction=stepper.BACKWARD, style = stepper.DOUBLE)
        except: print('\n' + str(round(time.time(),5)) + ': WHOOPS SKIPPED A STEP2r')


def stepper_operate(kitstepper1,kitstepper2, mode = None):

    print('starting main stepper function')

    #Declare vars
    start_time = time.time()
    file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    time_interval = 1 #seconds between settings changes
    if mode is None: mode = 'IDLE'
    resp_rate = 30
    tidal_vol = 500
    max_vol = 500
    peep = 10
    full_stroke_steps = 200
    ItoE_ratio= 0.5
    inspiratory_time = 1.0
    delay = inspiratory_time/1000  #was 0.001
    inhale_pressure_threshold = -100#Pa
    Pa2cmH2O = 1.0/98.0665
    assist_control = False
    delay_delta = 0
    last_mode = 'IDLE'
    time_per_step = 1.0/200

    #ONLY FOR DATA LOGGING
    import csv
    the_start = time.time() #time since power on bias
    data_start_time = time.time() #variable for data logging time tracking 
    data_time_interval = 30 #seconds between settings changes
    log_data = {'ontime':[],'time':[],'status':[]}

    #connect socket
    try:
        host = socket.gethostname()
        port = 12345     # BARO, the same port as used by the server
        baro = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        baro.connect((host, port))
        print('SOCKET INIT SUCCESS')
    except:
        print('UNABLE TO CONNECT TO BARO')
        baro = None



    while True:


        if mode == 'CHECKOUT':
            print('running stepper checkout...')

            steps = full_stroke_steps

            start_time = time.time()
            extend(kitstepper1,kitstepper2,steps,delay)
            stroke_in_time = time.time()-start_time

            print(str(round(time.time(),5))+': ' + str(steps) + ' steps in took: ' + str(round(stroke_in_time,4))+' s')

            time.sleep(0.5)

            steps = full_stroke_steps
            start_time = time.time()
            retract(kitstepper1,kitstepper2,steps,delay)
            stroke_out_time = time.time()-start_time

            print(str(round(time.time(),5))+': ' + str(steps) + ' steps out took: ' + str(round(stroke_out_time,4))+' s')

            #release rotor
            kitstepper1.release()
            kitstepper2.release()

            #stroke time
            stroke_time = (stroke_in_time+stroke_out_time)/2
            time_per_step = stroke_in_time/steps

            mode = 'IDLE'


        if mode == 'OPERATE':

            #math out the rest of the params
            #the_steps = int((full_stroke_steps/max_vol)*tidal_vol)
            the_steps = int(-0.0002*(tidal_vol**2)+0.4045*tidal_vol+71.536) #empirically derived
            est_stroke_time = time_per_step*the_steps

            exhal_time = est_stroke_time/ItoE_ratio
            if exhal_time < time_per_step*the_steps: exhal_time = time_per_step*the_steps+0.1

            total_breath_time = est_stroke_time+exhal_time

            rr_delay = (60/resp_rate)-total_breath_time
            if rr_delay < 0: rr_delay = 0


            #run w/ pause for appropriate RR
            sstart_time = time.time()
            log_data['ontime'] += [time.time()-the_start]
            log_data['time'] += [time.time()]
            log_data['status'] += [1]
            extend(kitstepper1,kitstepper2,the_steps,delay,meas_per_cycle=3,Baro=baro)
            log_data['ontime'] += [time.time()-the_start]
            log_data['time'] += [time.time()]
            log_data['status'] += [0]

            stroke_in_time = time.time()-sstart_time
            #print(str(round(time.time(),5))+': ' + str(the_steps) + ' steps in took: ' + str(round(stroke_in_time,4))+' s')

            #maybe should put a tiny pause here?
            log_data['ontime'] += [time.time()-the_start]
            log_data['time'] += [time.time()]
            log_data['status'] += [-1]
            retract(kitstepper1,kitstepper2,the_steps,delay)
            log_data['ontime'] += [time.time()-the_start]
            log_data['time'] += [time.time()]
            log_data['status'] += [0]

            #release rotor at end of stroke
            starttt = time.time()
            kitstepper1.release()
            kitstepper2.release()



            #wait and monitor baro
            dstart = time.time()
            loop_time = 0
            delay_time = 0.1 #seconds
            breath_flag = 0
            n = 0
            while loop_time <= rr_delay:

                loop_start = time.time()
                time.sleep(delay_time)

                if assist_control == True:

                    #pull baro data if it's there
                    bflag = 0
                    try:
                        the_start_time = time.time()
                        data_baro = baro.recv(10000)
                        bflag = 1
                    except:
                        print('SOCKET RECEIVE FAIL')

                    if bflag == 1:
                        qwe = data_baro.decode('ascii')
                        asd = qwe.split('}')  #catch multiple messages in the same read
                        baro_data = []
                        for item in asd:
                            if len(item) > 0:
                                baro_data += [item+'}']
                        bflag = 0
                        try:
                            data_baro= ast.literal_eval(baro_data[-1])
                            bflag = 1
                        except: 
                            try:
                                data_baro = ast.literal_eval(baro_data[-2])
                                bflag = 1
                            except: print('SOCKET DATA READ FAIL')

                        the_end_time = time.time()
                        if bflag == 1:
                            #print(data_baro)
                            try:
                                if data_baro['delta_pressure'] < inhale_pressure_threshold:
                                    print('BREATH DETECTED')
                                    print(data_baro['delta_pressure'])
                                    breath_flag = 1
                            except:
                                print('DELTA_PRESSURE KEY NOT FOUND')
                                try: print(data_baro)
                                except: print('UNABLE TO PRINT RECEIVED DATA')

                n+=1
                if n%10 == 0:
                    try:
                        with open(os.path.join(file_dir,'commands_to_stepper.json')) as f:
                            data = json.load(f)
                        mode = data['mode']
                        resp_rate = int(data['resp_rate'])
                        tidal_vol = int(data['tidal_vol'])
                        peep = int(data['peep'])

                    except: pass


                if breath_flag == 1: loop_time = rr_delay+1
                else: loop_time += (time.time()-loop_start)

            remainder = rr_delay - loop_time
            if remainder > 0: time.sleep(remainder)



            #compensate for true time
            delay_delta = 0
            try:
                tgt_cycle_time = 60/resp_rate
                true_cycle_time2 = time.time()-the_first_time2
                delay_delta = tgt_cycle_time-true_cycle_time2
                if delay_delta > 0 and breath_flag == 0: time.sleep(delay_delta)
                the_first_time2 = time.time()
            except:
                the_first_time2 = time.time()
                pass




        else: #idle
            time.sleep(1)


        #check for new inputs and write status
        if time.time()-start_time > time_interval:

            try:
                with open(os.path.join(file_dir,'commands_to_stepper.json')) as f:
                    data = json.load(f)
                mode = data['mode']
                resp_rate = int(data['resp_rate'])
                tidal_vol = int(data['tidal_vol'])
                peep = int(data['peep'])
                assist_control = data['assist_control']
                ItoE_ratio = float(data['ItoE_ratio'])
                inspiratory_time = float(data['inspiratory_time'])

                #only want to cal once
                if last_mode == 'CALIBRATE':
                    mode = 'IDLE'
                    last_mode = mode
                else: last_mode = mode

                #update other vars
                delay = inspiratory_time/1000


            except: pass

            status_dict = {'delay_delta':delay_delta, 'mode':data['mode']}
            with open(os.path.join(file_dir,'stepper_status.json'), 'w') as f:
                json.dump(status_dict, f)

            start_time = time.time()


        # save data less frequently
        if time.time()-data_start_time > data_time_interval:

            now = str(datetime.now())
            now = now.split('.')
            now = now[0]
            now = now.replace(' ','_')
            now = now.replace(':','-')

            keys = sorted(log_data.keys())
            with open(os.path.join(os.path.dirname(file_dir),'analysis','data','TEST_STEPPER_DATA_'+now+'.csv'),'w') as csv_file:
               writer=csv.writer(csv_file)
               writer.writerow(keys)
               writer.writerows(zip(*[log_data[key] for  key in keys]))

            log_data['ontime'] = []
            log_data['time'] = []
            log_data['status'] = []
            data_start_time = time.time()







###########################
#MAIN CODE
###########################

# initialize stepper
try:
    kit = MotorKit()
except:
    print('FAILED TO INIT MOTORKIT(), retrying...')
    time.sleep(2)
    kit = MotorKit()

#release rotor
kit.stepper1.release()
kit.stepper2.release()

atexit.register(turnOffMotors)


if __name__ == '__main__':

    
    try: args = sys.argv[1]
    except: args = None

    if args == 'MAIN': stepper_operate(kit.stepper1,kit.stepper2)
    else: stepper_operate(kit.stepper1,kit.stepper2, mode = 'OPERATE')



