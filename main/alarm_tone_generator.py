#!/usr/bin/python3

'''
alarm_tone_generator.py

This program is intended to generate
the alarm tone

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

import numpy as np
import simpleaudio as sa
from scipy.io.wavfile import write

#vent IEC alarm pattern: C4 A4 F4 - A4 F4
#C4	261.63 	131.87
#A4	440.00 	78.41
#F4	349.23 	98.79

samplerate = 44100  # 44100 samples per second

#frequency = 440  # Our played note will be 440 Hz
#seconds = 3  # Note duration of 3 seconds
#t = np.linspace(0, seconds, seconds * samplerate, False)
#data = np.sin(frequency * t * 2 * np.pi)

secs = 0.3
note1 = np.linspace(0, secs, secs * samplerate, False)
data1 = np.sin(261.63 * note1 * 2 * np.pi)

secs = 0.3
note2 = np.linspace(0, secs, secs * samplerate, False)
data2 = np.sin(440.00 * note2 * 2 * np.pi)

secs = 0.3
note3 = np.linspace(0, secs, secs * samplerate, False)
data3 = np.sin(349.23 * note3 * 2 * np.pi)

secs = 0.4
note4 = np.linspace(0, secs, secs * samplerate, False)
data4 = np.sin(0.00 * note4 * 2 * np.pi)

secs = 0.3
note5 = np.linspace(0, secs, secs * samplerate, False)
data5 = np.sin(440.00 * note5 * 2 * np.pi)

secs = 0.3
note6 = np.linspace(0, secs, secs * samplerate, False)
data6 = np.sin(349.23 * note6 * 2 * np.pi)

data = np.append(data1, data2)
data = np.append(data, data3)
data = np.append(data, data4)
data = np.append(data, data5)
data = np.append(data, data6)


sound = data * (2**15 - 1) / np.max(np.abs(data))

# Convert to 16-bit data
sound = sound.astype(np.int16)

write("alarm_sound.wav", samplerate, sound)

# Start playback
play_obj = sa.play_buffer(sound, 1, 2, samplerate)

# Wait for playback to finish before exiting
play_obj.wait_done()