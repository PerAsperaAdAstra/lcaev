#!/usr/bin/python3

'''
GUI_text.py

This script contains the strings used
for the GUI and allows for multi-language
support.

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''


def cal_label_text(language):
    if language == 'Espanol':
        the_string = 'CALIBRACION'

    elif language == 'Italiano':
        the_string = 'CALIBRAZIONE'

    else:
        the_string = "CALIBRATION"
    return the_string


def ensure_disconnect_text(language):
    if language == 'Espanol':
        the_string = 'DESCONECTADO DEL PACIENTE'

    elif language == 'Italiano':
        the_string = 'SCOLLEGARE DAL PAZIENTE'

    else:
        the_string = "ENSURE SYSTEM IS DISCONNECTED FROM PATIENT"
    return the_string


def start_cal_text(language):
    if language == 'Espanol':
        the_string = 'INICIAR CALIBRACION'

    elif language == 'Italiano':
        the_string = 'INIZIA CALIBRAZIONE'

    else:
        the_string = "START CALIBRATION"
    return the_string

def self_test_text(language):
    if language == 'Espanol':
        the_string = 'Autotest'

    elif language == 'Italiano':
        the_string = 'Autotest'

    else:
        the_string = "Unit Self Test"
    return the_string


def start_test_text(language):
    if language == 'Espanol':
        the_string = 'INICIAR PRUEBA'

    elif language == 'Italiano':
        the_string = 'INIZIA AUTOTEST'

    else:
        the_string = "START SELF TEST"
    return the_string


def wait_text(language):
    if language == 'Espanol':
        the_string = 'Espere..'

    elif language == 'Italiano':
        the_string = 'Attendere...'

    else:
        the_string = "Please wait..."
    return the_string


def settings_text(language):
    if language == 'Espanol':
        the_string = 'AJUSTE'

    elif language == 'Italiano':
        the_string = 'REGOLAZIONE'

    else:
        the_string = "SETTINGS"
    return the_string


def continue_text(language):
    if language == 'Espanol':
        the_string = 'Continuar'

    elif language == 'Italiano':
        the_string = 'Continua'

    else:
        the_string = "Continue"
    return the_string


def retry_text(language):
    if language == 'Espanol':
        the_string = 'Rever'

    elif language == 'Italiano':
        the_string = 'Retry'

    else:
        the_string = "Retry"
    return the_string


def flow_prompt_text(language):
    if language == 'Espanol':
        the_string = 'FLUYE EL AIRE?'

    elif language == 'Italiano':
        the_string = "L'ARIA E IN FLUSSO"

    else:
        the_string = "IS AIR FLOWING"
    return the_string

def flow_prompt_text2(language):
    if language == 'Espanol':
        the_string = 'Y SONIDO DE ALARMA?'

    elif language == 'Italiano':
        the_string = "E SUONA L'ALLARME?"

    else:
        the_string = "AND ALARM SOUNDING?"
    return the_string


def yes_text(language):
    if language == 'Espanol':
        the_string = 'SI'

    elif language == 'Italiano':
        the_string = 'SI'

    else:
        the_string = "YES"
    return the_string


def no_text(language):
    if language == 'Espanol':
        the_string = 'NO'

    elif language == 'Italiano':
        the_string = 'NO'

    else:
        the_string = "NO"
    return the_string

def failure_text(language):
    if language == 'Espanol':
        the_string = 'FALLA DEL SISTEMA CRITICO'

    elif language == 'Italiano':
        the_string = 'GUASTO DEL SISTEMA CRITICO'

    else:
        the_string = "CRITICAL SYSTEM FAILURE"
    return the_string

def unit_fail_text(language):
    if language == 'Espanol':
        the_string = 'FALLA DE LA UNIDAD'

    elif language == 'Italiano':
        the_string = 'MANCATO UNITA'

    else:
        the_string = "UNIT FAILURE"
    return the_string

def baro_cal_failure_text(language):
    if language == 'Espanol':
        the_string = 'BARO CAL FRACASO'

    elif language == 'Italiano':
        the_string = 'BARO CAL DIFETTO'

    else:
        the_string = "BARO CAL FAILURE"
    return the_string


def do_not_use_text(language):
    if language == 'Espanol':
        the_string = 'NO LO USE - RECIBA INMEDIATAMENTE'

    elif language == 'Italiano':
        the_string = 'NON UTILIZZARE - RICEVERE SUBITO UNA MANUTENZIONE'

    else:
        the_string = "DO NOT USE -- GET SERVICED IMMEDIATELY"
    return the_string

def test_pass_text(language):
    if language == 'Espanol':
        the_string = 'PASO DE PRUEBA'

    elif language == 'Italiano':
        the_string = 'TEST PASS'

    else:
        the_string = "TEST PASS"
    return the_string


def ready_text(language):
    if language == 'Espanol':
        the_string = 'LISTO PARA OPERAR'

    elif language == 'Italiano':
        the_string = 'PRONTO PER OPERARE'

    else:
        the_string = "READY TO OPERATE"
    return the_string


def next_text(language):
    if language == 'Espanol':
        the_string = 'SIGUIENTE'

    elif language == 'Italiano':
        the_string = 'PROSSIMO'

    else:
        the_string = "NEXT"
    return the_string


def mute_text(language):
    if language == 'Espanol':
        the_string = 'SILENCIO'

    elif language == 'Italiano':
        the_string = 'SILENZIO'

    else:
        the_string = "MUTE"
    return the_string

def start_text(language):
    if language == 'Espanol':
        the_string = 'COMIENZO'

    elif language == 'Italiano':
        the_string = 'INIZIO'

    else:
        the_string = "START"
    return the_string

def stop_text(language):
    if language == 'Espanol':
        the_string = 'ALTO'

    elif language == 'Italiano':
        the_string = 'STOP'

    else:
        the_string = "STOP"
    return the_string





