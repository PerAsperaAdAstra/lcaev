#!/usr/bin/python3

#socket client demo

#import module
import os
import ast
import csv
import time
import socket
import shutil
import select
from datetime import datetime


#define vars
start=  time.time()
rstart = time.time()
time_interval =   10 #sec

# define lists
blogtime = []
btime=[]
btemp=[]
bpress=[]


#connect
try:
    host = socket.gethostname()
    port = 12345     # BARO              # The same port as used by the server
    baro = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    baro.connect((host, port))
    #s.sendall(b'Hello, world')
    print('SOCKET INIT SUCCESS')
except:
    print('SOCKET INIT FAIL')
    pass



#continually do the thing
while True:

    print('loopin')

    #time.sleep(2)

    end  = time.time()

#pull data if it's there

#baro data
    bflag = 0
    try:
        #r, _, _ = select.select([self.conn], [], [])
        #if r:
            # ready to receive
            #message = self.conn.recv(1024)
        the_start_time = time.time()
        data_baro = baro.recv(1024)
        #print('Received', repr(data_baro))
        bflag = 1
    except:
        print('SOCKET RECEIVE FAIL')
        pass
    if bflag == 1:
        qwe = data_baro.decode('ascii')
        asd = qwe.split('}')  #catch multiple messages in the same read
        baro_data = []
        for item in asd:
            if len(item) > 0:
                baro_data += [item+'}']
        for data in baro_data:
            data_baro = ast.literal_eval(data)
            the_end_time = time.time()
            blogtime += [time.time()]
            btime+=[data_baro['time']]
            btemp+=[data_baro['temp']]
            bpress+=[data_baro['pressure']]



        #reset  time
        start  = time.time()

    if len(btime) > 0:
        print('\ncurrent time: ' + str(time.time()))
        print('packet time: ' + str(btime[-1]))
        print('read time: ' + str(the_end_time-the_start_time))
        btime = []



