#!/usr/bin/python3

'''
system_data_logger.py

This program is intended to 
log system data

Set up for a python3 on a pi 4

'''

###########################
#IMPORT MODULES
###########################
import os
import csv
import time
import json
import psutil
from datetime import datetime
from gpiozero import CPUTemperature



###########################
#MAIN CODE
###########################

#vars

VERBOSE = False


time_interval = 30
file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
ontime = []
trutime = []

pcpu = []
pram = []
tcpu = []

the_start = time.time()
start_time = time.time()

cpu = CPUTemperature()


while True:

    bam = time.time()
    ontime += [time.time()-the_start]
    trutime += [time.time()]
    bam2 = time.time()
    tcpu += [cpu.temperature]
    pcpu += [psutil.cpu_percent(1)] #since we're not reading a baro, take your time
    pram += [psutil.virtual_memory()[2]]
    if VERBOSE:
        print('\n----------------------------------')
        print('read time: ' + str(bam2-bam)+' s')
        print(tcpu[-1])
        print(pcpu[-1])
        print(pram[-1])
   #time.sleep(0.1)


    #check for new inputs
    if time.time()-start_time > time_interval:

        now = str(datetime.now())
        now = now.split('.')
        now = now[0]
        now = now.replace(' ','_')
        now = now.replace(':','-')

        data = {'Ontime':ontime,'Time':trutime,'CPU temp':tcpu,'CPU Percent':pcpu,'RAM Percent':pram}

        keys = sorted(data.keys())
        with open(os.path.join(os.path.dirname(file_dir),'analysis','data','TEST_SYS_DATA_'+now+'.csv'),'w') as csv_file:
             writer=csv.writer(csv_file)
             writer.writerow(keys)  
             writer.writerows(zip(*[data[key] for  key in keys]))



        start_time = time.time()
        ontime = []
        trutime = []
        pcpu = []
        pram = []
        tcpu = []

        if VERBOSE:
            print('\n\n\n\nBLAM! written.\n\n\n\n')
        time.sleep(5)


