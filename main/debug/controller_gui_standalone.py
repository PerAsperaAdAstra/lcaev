#!/usr/bin/python3

'''
controller_gui.py

This program is intended to provide a
Graphical User Interface so users
can operate the LCAEV

built for RPi4 with a 7" 1920x1080
downscaled screen

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
print('load modules')
import sys
import os
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
import time
import json
import psutil
import gui_text as gt
#import Tkinter as tk python 2
import tkinter as tk #python 3
import subprocess


###########################
#CLASSES
###########################

#GUI class
class theGUI:

    def __init__(self, master):

        # set vars
        print('define vars within class')
        self.master = master
        master.title("EMERGENCY VENTILATOR CONTROL SYSTEM")
        self.GO_DICT = {'test':False}
        self.ARMED_STATUS = False
        self.standard_font = 'Helvetica 100 bold'
        self.sorta_small_font = 'Helvetica 60 bold'
        self.small_font = 'Helvetica 30 bold'
        self.huge_font = 'Helvetica 150 bold'
        self.language_options = ['English','Espanol','Italiano']
        self.old_alarm_dict = {}
        self.old_stepper_dict = {}
        self.start_button_state = "normal"
        self.start_button_color = "green2"
        self.stop_button_state = "disable"
        self.stop_button_color = "gray"

        # SET VENTILATOR VARS
        self.RUNNING = False
        self.RR = 10
        self.Vt = 500
        self.PEEP = 10
        self.MUTE = 0
        self.stepper_mode = 'IDLE'
        self.alarm_mode = 'IDLE'
        self.baro_mode = 'IDLE'

        self.assist_control = True
        #self.inspiratory_time_options = [1.0,1.5,2.0]
        self.inspiratory_time_options = [1.5,1.5,1.5]  #placeholders until successful verification
        self.inspiratory_time = self.inspiratory_time_options[0]

        self.ItoE_ratio_options_str = ['1:1','1:2','1:3']
        self.ItoE_ratio_options = [1.0,0.5,0.33]
        self.ItoE_ratio = self.ItoE_ratio_options[1]



       # creating a menu instance
        menu = tk.Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = tk.Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.close_program)

        # added "file" to our menu
        menu.add_cascade(label="File", menu=file)

        #full file dir
        self.full_file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

        # clean prior config and status files and create new ones
        try: os.remove(os.path.join(self.full_file_dir,'commands_to_stepper.json'))
        except: pass

        try: os.remove(os.path.join(self.full_file_dir,'commands_to_alarm.json'))
        except: pass

        try: os.remove(os.path.join(self.full_file_dir,'commands_to_baro.json'))
        except: pass

        try: os.remove(os.path.join(self.full_file_dir,'stepper_status.json'))
        except: pass

        try: os.remove(os.path.join(self.full_file_dir,'baro_status.json'))
        except: pass


        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP, 'assist_control':self.assist_control, 'inspiratory_time':self.inspiratory_time,'ItoE_ratio':self.ItoE_ratio}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        baro_dict = {'mode':self.baro_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt}

        with open(os.path.join(self.full_file_dir,'commands_to_stepper.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'commands_to_alarm.json'), 'w') as f:
            json.dump(alarm_dict, f)
        with open(os.path.join(self.full_file_dir,'commands_to_baro.json'), 'w') as f:
            json.dump(baro_dict, f)


        #clean prior instances of support scripts
        for proc in psutil.process_iter():
            if proc.name() == 'python3':
                if len(proc.cmdline()) > 1:
                    script_path = ' '.join(proc.cmdline())
                    if '_control.py' in script_path:
                        print('killing ' + str(script_path))
                        proc.terminate()


        #start support scripts
        with open(os.path.join(self.full_file_dir,'baro_out.txt'), "w") as outfile1:
            subprocess.Popen(['python3 -u '+os.path.join(self.full_file_dir,'baro_control.py ') + 'MAIN'], shell=True, stdin=None, stdout=outfile1, stderr=outfile1)
        time.sleep(2) #wait for socket setup

        with open(os.path.join(self.full_file_dir,'stepper_out.txt'), "w") as outfile2:
            subprocess.Popen(['python3 -u '+os.path.join(self.full_file_dir,'stepper_control.py ') + 'MAIN'], shell=True, stdin=None, stdout=outfile2, stderr=outfile2)

        with open(os.path.join(self.full_file_dir,'alarm_out.txt'), "w") as outfile3:
            subprocess.Popen(['python3 -u '+os.path.join(self.full_file_dir,'alarm_control.py ') + 'MAIN'], shell=True, stdin=None, stdout=outfile3, stderr=outfile3)

        #FOR TEMPORARY DATA LOGGING ONLY
        subprocess.Popen(['python3 -u '+os.path.join(self.full_file_dir,'system_data_logger.py ')], shell=True, stdin=None, stdout=None, stderr=None)

        # draw language selection page
        self.draw_language_prompt()


    # step 1, language select
    def draw_language_prompt(self):

        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("            ")
        self.spacer1 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer1.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("Select Language")
        self.lang_select_label = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.lang_select_label.grid(row=roww,column=1)

        roww+=1
        self.language_var = tk.StringVar()
        self.language_var.set(self.language_options[0]) # default value
        self.language_menu = tk.OptionMenu(self.master, self.language_var, *self.language_options)
        self.language_menu.grid(row=roww,column=1)
        self.language_menu.configure(font=self.standard_font)
        self.language_menu['menu'].configure(font=self.standard_font)

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("  ")
        self.spacer2 = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.spacer2.grid(row=roww,column=0)

        roww+=1
        self.lang_confirm_button = tk.Button(self.master, text="CONFIRM", font=self.standard_font, command= self.lang_confirm_button_cmd)
        self.lang_confirm_button.grid(row=roww,column=1)
        self.lang_confirm_button.config(bg='green2')


    def lang_confirm_button_cmd(self):

        #need to clean up all widgets and then call next draw
        self.language_menu.grid_forget()
        self.lang_select_label.grid_forget()
        self.lang_confirm_button.grid_forget()
        self.spacer1.grid_forget()
        self.spacer2.grid_forget()

        self.draw_calibration_window()


    # step 2, calibrate sensors
    def draw_calibration_window(self):


        #clean up cal retry widgets if we're coming from there
        try:
            self.spacer5.grid_forget()
            self.cal_fail_label.grid_forget()
            self.cal_fail_label2.grid_forget()
            self.cal_retry_button.grid_forget()
            self.cal_skip_button.grid_forget()
        except: pass

        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("       ")
        self.spacer3 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer3.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.cal_label_text(self.language_var.get()) + " :") #CALIBRATION
        self.cal_caution_label = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.cal_caution_label.grid(row=roww,column=1)
        self.cal_caution_label.config(bg='light yellow')

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.ensure_disconnect_text(self.language_var.get())) #ENSURE SYSTEM IS DISCONNECTED FROM PATIENT
        self.cal_caution_label2 = tk.Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.cal_caution_label2.grid(row=roww,column=1)
        self.cal_caution_label2.config(bg='light yellow')

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(" ")
        self.spacer4 = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.spacer4.grid(row=roww,column=0)

        roww+=1
        self.cal_confirm_button = tk.Button(self.master, text=gt.start_cal_text(self.language_var.get()), font=self.standard_font, command= self.cal_confirm_button_cmd) #START CALIBRATION
        self.cal_confirm_button.grid(row=roww,column=1)
        self.cal_confirm_button.config(bg='green2')


    def cal_confirm_button_cmd(self):

        self.spacer3.grid_forget()

        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("             ")
        self.spacer3 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer3.grid(row=roww,column=0)

        #update button state
        self.cal_confirm_button['state'] = "disable"
        self.cal_confirm_button['text'] = gt.wait_text(self.language_var.get())


        #calibrate baro
        self.baro_mode = 'CALIBRATE'
        baro_dict = {'mode':self.baro_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt}
        with open(os.path.join(self.full_file_dir,'commands_to_baro.json'), 'w') as f:
            json.dump(baro_dict, f)

        self.master.update()
        self.cal_confirm_cleanup()



    def cal_confirm_cleanup(self):

        cal_complete = False
        try: os.remove(os.path.join(self.full_file_dir,'baro_status.json'))
        except: pass

        while cal_complete == False:

            time.sleep(1)
            found_file = False
            while found_file == False:
                try:
                    with open(os.path.join(self.full_file_dir,'baro_status.json')) as f:
                        data = json.load(f)
                    cal_complete = data['cal_complete']
                    found_file = True
                except: pass


        #advance baro to operate
        self.baro_mode = 'OPERATE'
        baro_dict = {'mode':self.baro_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt}
        with open(os.path.join(self.full_file_dir,'commands_to_baro.json'), 'w') as f:
            json.dump(baro_dict, f)

        #need to clean up all widgets and then call next draw
        self.spacer3.grid_forget()
        self.spacer4.grid_forget()
        self.cal_caution_label.grid_forget()
        self.cal_caution_label2.grid_forget()
        self.cal_confirm_button.grid_forget()

        if cal_complete == True:
            self.draw_self_test_start_window()
        elif cal_complete == 'FAILURE':
            print('BARO CAL FAILURE')
            try: os.remove(os.path.join(self.full_file_dir,'baro_status.json'))
            except: pass
            self.draw_cal_fail_window()
        elif cal_complete == 'NO BARO':
            print('BARO NOT FOUND')
            try: os.remove(os.path.join(self.full_file_dir,'baro_status.json'))
            except: pass
            self.draw_cal_fail_window()
        else:
            try: os.remove(os.path.join(self.full_file_dir,'baro_status.json'))
            except: pass
            self.draw_cal_fail_window()
            print('UNKNOWN BARO CAL FAILURE')


    # step 2b, draw baro fail window
    def draw_cal_fail_window(self):

        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("        ")
        self.spacer5 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer5.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.baro_cal_failure_text(self.language_var.get())) #Unit Self Test
        self.cal_fail_label = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.cal_fail_label.grid(row=roww,column=1)

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.ensure_disconnect_text(self.language_var.get())) #ENSURE SYSTEM IS DISCONNECTED FROM PATIENT
        self.cal_fail_label2 = tk.Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.cal_fail_label2.grid(row=roww,column=1)
        self.cal_fail_label2.config(bg='light yellow')

        roww+=1
        self.cal_retry_button = tk.Button(self.master, text=gt.retry_text(self.language_var.get()), font=self.standard_font, command= self.draw_calibration_window) #RETRY
        self.cal_retry_button.grid(row=roww,column=1)
        self.cal_retry_button.config(bg='green2')

        roww+=1
        self.cal_skip_button = tk.Button(self.master, text=gt.continue_text(self.language_var.get()), font=self.standard_font, command= self.draw_self_test_start_window) #CONTINUE
        self.cal_skip_button.grid(row=roww,column=1)
        self.cal_skip_button.config(bg='red')


    # step 3, self-test start
    def draw_self_test_start_window(self):

        #clean up cal retry widgets if we're coming from there
        try:
            self.spacer5.grid_forget()
            self.cal_fail_label.grid_forget()
            self.cal_fail_label2.grid_forget()
            self.cal_retry_button.grid_forget()
            self.cal_skip_button.grid_forget()
        except: pass


        #spacer
        roww = 0
        self.label_text5 = tk.StringVar()
        self.label_text5.set("          ")
        self.spacer5 = tk.Label(self.master, textvariable=self.label_text5, font=self.standard_font)
        self.spacer5.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.self_test_text(self.language_var.get())) #Unit Self Test
        self.self_test_label = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.self_test_label.grid(row=roww,column=1)

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.ensure_disconnect_text(self.language_var.get())) #ENSURE SYSTEM IS DISCONNECTED FROM PATIENT
        self.self_test_label2 = tk.Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.self_test_label2.grid(row=roww,column=1)
        self.self_test_label2.config(bg='light yellow')

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("  ")
        self.spacer6 = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.spacer6.grid(row=roww,column=0)

        roww+=1
        self.self_test_start_button = tk.Button(self.master, text=gt.start_test_text(self.language_var.get()), font=self.standard_font, command= self.test_start_button_cmd) #START SELF TEST
        self.self_test_start_button.grid(row=roww,column=1)
        self.self_test_start_button.config(bg='green2')


    def test_start_button_cmd(self):


        self.label_text5.set("            ")

        #update button state
        self.self_test_start_button['state'] = "disable"
        self.self_test_start_button['text'] = gt.wait_text(self.language_var.get())


        self.master.update()

        #move on to next window
        self.test_confirm_cleanup()


    def test_confirm_cleanup(self):

        #subprocess the test procedures
        self.stepper_mode = 'CHECKOUT'
        self.alarm_mode = 'CHECKOUT'
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP, 'assist_control':self.assist_control, 'inspiratory_time':self.inspiratory_time,'ItoE_ratio':self.ItoE_ratio}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'commands_to_stepper.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'commands_to_alarm.json'), 'w') as f:
            json.dump(alarm_dict, f)


        test_start = False

        while test_start == False:

            time.sleep(1)
            try:
                with open(os.path.join(self.full_file_dir,'stepper_status.json')) as f:
                    data = json.load(f)
                mode = data['mode']
                if mode == 'CHECKOUT':
                    test_start = True
            except: pass


        #call next draw
        self.draw_self_test_complete_window()






    def draw_self_test_complete_window(self):

        #move on to next window
        self.draw_self_test_complete_window()



    # step 3, self-test complete
    def draw_self_test_complete_window(self):

        #need to clean up all widgets and then call next draw
        self.spacer5.grid_forget()
        self.spacer6.grid_forget()
        self.self_test_label.grid_forget()
        self.self_test_label2.grid_forget()
        self.self_test_start_button.grid_forget()


        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("    ")
        self.spacer7 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer7.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.flow_prompt_text(self.language_var.get())) #IS AIR FLOWING?
        self.self_test_label3 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.self_test_label3.grid(row=roww,column=1)

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.flow_prompt_text2(self.language_var.get()))
        self.self_test_label4 = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.self_test_label4.grid(row=roww,column=1)

        roww+=1
        self.self_test_good_button = tk.Button(self.master, text=gt.yes_text(self.language_var.get()), font=self.standard_font, command= self.test_pass_button_cmd) #YES
        self.self_test_good_button.grid(row=roww,column=1)
        self.self_test_good_button.config(bg='green2')

        roww+=1
        self.self_test_fail_button = tk.Button(self.master, text=gt.no_text(self.language_var.get()), font=self.standard_font, command= self.test_fail_button_cmd) #NO
        self.self_test_fail_button.grid(row=roww,column=1)
        self.self_test_fail_button.config(bg='red')


    def test_pass_button_cmd(self):

        #return systems to idle
        self.stepper_mode = 'IDLE'
        self.alarm_mode = 'IDLE'
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP, 'assist_control':self.assist_control, 'inspiratory_time':self.inspiratory_time,'ItoE_ratio':self.ItoE_ratio}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'commands_to_stepper.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'commands_to_alarm.json'), 'w') as f:
            json.dump(alarm_dict, f)


        #need to clean up all widgets and then call next draw
        self.spacer7.grid_forget()

        self.self_test_label3.grid_forget()
        self.self_test_label4.grid_forget()
        self.self_test_good_button.grid_forget()
        self.self_test_fail_button.grid_forget()

        self.draw_self_test_pass_window()


    def test_fail_button_cmd(self):

        #need to clean up all widgets and then call next draw
        self.self_test_label3.grid_forget()
        self.self_test_label4.grid_forget()
        self.self_test_good_button.grid_forget()
        self.self_test_fail_button.grid_forget()

        self.draw_test_fail_window()


    # step fail window
    def draw_test_fail_window(self):
        print(gt.failure_text(self.language_var.get())) #CRITICAL FAILURE
        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("               ")
        self.label = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.label.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.unit_fail_text(self.language_var.get())) #UNIT FAILURE
        self.self_test_label = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.self_test_label.grid(row=roww,column=1)
        self.self_test_label.config(bg='red')

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.do_not_use_text(self.language_var.get())) #DO NOT USE -- GET SERVICED IMMEDIATELY
        self.self_test_label2 = tk.Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.self_test_label2.grid(row=roww,column=1)
        self.self_test_label2.config(bg='light yellow')

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("  ")
        self.label = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.label.grid(row=roww,column=0)


    # draw test pass window
    def draw_self_test_pass_window(self):

        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("                ")
        self.spacer9 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer9.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.test_pass_text(self.language_var.get())) #TEST PASS
        self.self_test_label3 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.self_test_label3.grid(row=roww,column=1)

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(gt.ready_text(self.language_var.get())) #READY TO OPERATE
        self.self_test_label4 = tk.Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.self_test_label4.grid(row=roww,column=1)
        self.self_test_label4.config(bg='light yellow')

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("  ")
        self.spacer10 = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.spacer10.grid(row=roww,column=0)

        roww+=1
        self.self_test_next_button = tk.Button(self.master, text=gt.next_text(self.language_var.get()), font=self.standard_font, command= self.draw_operate_screen)
        self.self_test_next_button.grid(row=roww,column=1)
        self.self_test_next_button.config(bg='green2')




    # draw operate screen
    def draw_operate_screen(self):

        # clean up prior screens
        self.spacer9.grid_forget()
        self.spacer10.grid_forget()

        self.self_test_label3.grid_forget()
        self.self_test_label4.grid_forget()
        self.self_test_next_button.grid_forget()


        #Top Row
        #spacers 
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("    ")
        self.spacer11 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer11.grid(row=roww,column=0)

        self.settings_button = tk.Button(self.master, text=gt.settings_text(self.language_var.get()), font=self.sorta_small_font, command= self.draw_settings_screen)
        self.settings_button.grid(row=roww,column=1)
        self.settings_button.config(bg='dark green')
        self.settings_button.config(fg='white')

        self.label_text = tk.StringVar()
        self.label_text.set("    ")
        self.spacer12 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer12.grid(row=roww,column=3)

        self.mute_button = tk.Button(self.master, text=gt.mute_text(self.language_var.get()), font=self.sorta_small_font, command= self.mute_alarm)
        self.mute_button.grid(row=roww,column=6)
        self.mute_button.config(bg='blue')
        self.mute_button.config(fg='white')


        #draw second row
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("RR")
        self.label_RR_name = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.label_RR_name.grid(row=roww,column=1)

        self.label_RR_text = tk.StringVar()
        self.label_RR_text.set(str(self.RR))
        self.label_RR = tk.Label(self.master, textvariable=self.label_RR_text,font=self.standard_font, relief=tk.SUNKEN)
        self.label_RR.grid(row=roww,column=2)
        self.label_RR.config(bg='white')


        self.label_text = tk.StringVar()
        self.label_text.set("Vt")
        self.label_Vt_name = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.label_Vt_name.grid(row=roww,column=4)

        self.label_Vt_text = tk.StringVar()
        self.label_Vt_text.set(str(self.Vt))
        self.label_Vt = tk.Label(self.master, textvariable=self.label_Vt_text,font=self.standard_font, relief=tk.SUNKEN)
        self.label_Vt.grid(row=roww,column=5)
        self.label_Vt.config(bg='white')


        #draw third row
        roww+=1
        self.RR_plus_button = tk.Button(self.master, text=" + ", font=self.huge_font, command= self.RR_plus)
        self.RR_plus_button.grid(row=roww,column=1)
        self.RR_plus_button.config(bg='light blue')

        self.RR_min_button = tk.Button(self.master, text=" - ", font=self.huge_font, command= self.RR_min)
        self.RR_min_button.grid(row=roww,column=2)
        self.RR_min_button.config(bg='light blue')



        self.Vt_plus_button = tk.Button(self.master, text=" + ", font=self.huge_font, command= self.Vt_plus)
        self.Vt_plus_button.grid(row=roww,column=4)
        self.Vt_plus_button.config(bg='light blue')

        self.Vt_min_button = tk.Button(self.master, text=" - ", font=self.huge_font, command= self.Vt_min)
        self.Vt_min_button.grid(row=roww,column=5)
        self.Vt_min_button.config(bg='light blue')


        #draw fourth row (just a spacer)
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("    ")
        self.spacer12 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer12.grid(row=roww,column=3)

        #draw fifth row
        roww+=1
        self.vent_start_button = tk.Button(self.master, text=gt.start_text(self.language_var.get()), font=self.standard_font, command= self.start_vent)
        self.vent_start_button['state'] = self.start_button_state
        self.vent_start_button.grid(row=roww,column=1)
        self.vent_start_button.config(bg=self.start_button_color )


        self.vent_stop_button = tk.Button(self.master, text=gt.stop_text(self.language_var.get()), font=self.standard_font, command= self.stop_vent)
        self.vent_stop_button['state'] = self.stop_button_state
        self.vent_stop_button.grid(row=roww,column=5)
        self.vent_stop_button.config(bg=self.stop_button_color)



    # draw settings screen
    def draw_settings_screen(self):

        # hide ops bits
        self.Vt_plus_button.grid_forget()
        self.Vt_min_button.grid_forget()
        self.RR_min_button.grid_forget()
        self.RR_plus_button.grid_forget()
        self.label_Vt.grid_forget()
        self.label_RR.grid_forget()
        self.label_Vt_name.grid_forget()
        self.label_RR_name.grid_forget()
        self.settings_button.grid_forget()
        self.vent_start_button.grid_forget()
        self.vent_stop_button.grid_forget()
        self.mute_button.grid_forget()


        #capture current vars
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP, 'assist_control':self.assist_control, 'inspiratory_time':self.inspiratory_time,'ItoE_ratio':self.ItoE_ratio}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        self.old_alarm_dict = alarm_dict.copy()
        self.old_stepper_dict = stepper_dict.copy()


        roww=0


        roww+=1
        if self.assist_control == True:
            self.assist_control_button = tk.Button(self.master, text='ASSIST CONTROL ON', font=self.sorta_small_font, command= self.assist_control_onoff)
            self.assist_control_button.grid(row=roww,column=1)
            self.assist_control_button.config(bg='green2')

        else:
            self.assist_control_button = tk.Button(self.master, text='ASSIST CONTROL OFF', font=self.sorta_small_font, command= self.assist_control_onoff)
            self.assist_control_button.grid(row=roww,column=1)
            self.assist_control_button.config(bg='red')


        #get baro status and disable AC if bad
        file_loaded = False
        while file_loaded == False:

            try:
                with open(os.path.join(self.full_file_dir,'baro_status.json')) as f:
                    bdata = json.load(f)
                file_loaded = True
            except: pass

            time.sleep(1)
        if bdata['baro_status'] != True:
            self.assist_control_button['text'] = "ASSIST CONTROL OFF"
            self.assist_control_button['state'] = "disable"
            self.assist_control_button.config(bg='gray')



        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("TI (sec)")
        self.label_TI_name = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.label_TI_name.grid(row=roww,column=1)

        self.iTime_middleman = tk.DoubleVar()
        self.iTime_middleman.set(self.inspiratory_time) # default value
        self.iTime_menu = tk.OptionMenu(self.master, self.iTime_middleman, *self.inspiratory_time_options)
        self.iTime_menu.grid(row=roww,column=2)
        self.iTime_menu.configure(font=self.standard_font)
        self.iTime_menu['menu'].configure(font=self.standard_font)

        roww+=1
        the_idx = self.ItoE_ratio_options.index(self.ItoE_ratio)
        self.ItoE_ratio = self.ItoE_ratio_options[1]

        self.label_text = tk.StringVar()
        self.label_text.set("I:E")
        self.label_IE_name = tk.Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.label_IE_name.grid(row=roww,column=1)

        self.ItoE_middleman = tk.StringVar()
        self.ItoE_middleman.set(self.ItoE_ratio_options_str[the_idx]) # default value
        self.ItoE_menu = tk.OptionMenu(self.master, self.ItoE_middleman, *self.ItoE_ratio_options_str)
        self.ItoE_menu.grid(row=roww,column=2)
        self.ItoE_menu.configure(font=self.standard_font)
        self.ItoE_menu['menu'].configure(font=self.standard_font)


        roww+=1
        self.apply_button = tk.Button(self.master, text='APPLY', font=self.standard_font, command= self.write_and_return_to_ops)
        self.apply_button.grid(row=roww,column=1)
        self.apply_button.config(bg='green2')

        self.back_button = tk.Button(self.master, text='CANCEL', font=self.standard_font, command= self.cancel_and_return_to_ops)
        self.back_button.grid(row=roww,column=2)
        self.back_button.config(bg='red')


    def cancel_and_return_to_ops(self):

        self.stepper_mode = self.old_stepper_dict['mode']
        self.RR = self.old_stepper_dict['resp_rate']
        self.Vt = self.old_stepper_dict['tidal_vol']
        self.PEEP = self.old_stepper_dict['peep']
        self.assist_control = self.old_stepper_dict['assist_control']
        self.inspiratory_time = self.old_stepper_dict['inspiratory_time']
        self.ItoE_ratio = self.old_stepper_dict['ItoE_ratio']

        self.alarm_mode = self.old_alarm_dict['mode']
        self.MUTE = self.old_alarm_dict['mute']

        self.clean_and_draw_operate_screen()

    def write_and_return_to_ops(self):

        self.inspiratory_time = self.iTime_middleman.get()

        the_idx = self.ItoE_ratio_options_str.index(self.ItoE_middleman.get())
        self.ItoE_ratio = self.ItoE_ratio_options[the_idx]

        self.write_config()
        self.clean_and_draw_operate_screen()

    def clean_and_draw_operate_screen(self):
        self.label_TI_name.grid_forget()
        self.label_IE_name.grid_forget()

        self.iTime_menu.grid_forget()
        self.ItoE_menu.grid_forget()
        self.assist_control_button.grid_forget()
        self.back_button.grid_forget()

        self.draw_operate_screen()



    # RR plus button command
    def RR_plus(self):

        #update vars
        self.RR += 1

        # add gate to prevent crazy high RR!
        if self.RR > 35:
            self.RR = 35

        #redraw label
        self.label_RR_text.set(str(self.RR))

        #write settings/state to file
        self.write_config()

    # RR minus button command
    def RR_min(self):

        #update vars
        self.RR -= 1

        # add gate to prevent negative RR!
        if self.RR < 1:
            self.RR = 1

        #redraw label
        self.label_RR_text.set(str(self.RR))

        #write settings/state to file
        self.write_config()



    # Vt plus button command
    def Vt_plus(self):

        #update vars
        self.Vt += 50

        # add gate to prevent crazy high RR!
        if self.Vt > 800: #swag system limit which may currently be 700mL?
            self.Vt = 800

        #redraw label
        self.label_Vt_text.set(str(self.Vt))

        #write settings/state to file
        self.write_config()

    # Vt minus button command
    def Vt_min(self):

        #update vars
        self.Vt -= 50

        # add gate to prevent negative Vt!
        if self.Vt < 100:
            self.Vt = 100

        #redraw label
        self.label_Vt_text.set(str(self.Vt))

        #write settings/state to file
        self.write_config()



    # START VENT!
    def start_vent(self):

        #update vars
        self.RUNNING = True
        self.stepper_mode = 'OPERATE'
        self.alarm_mode = 'OPERATE'


        #redraw label
        self.start_button_state = "disable"
        self.start_button_color = "gray"
        self.vent_start_button['state'] = self.start_button_state
        self.vent_start_button.config(bg=self.start_button_color)

        self.stop_button_state = "normal"
        self.stop_button_color = "red"
        self.vent_stop_button['state'] = self.stop_button_state
        self.vent_stop_button.config(bg=self.stop_button_color)

        #write settings/state to file
        self.write_config()



    # STOP VENT!
    def stop_vent(self):

        #update vars
        self.RUNNING = False
        self.stepper_mode = 'IDLE'
        self.alarm_mode = 'IDLE'

        #redraw label
        self.stop_button_state = "disable"
        self.stop_button_color = "gray"
        self.vent_stop_button['state'] = self.stop_button_state
        self.vent_stop_button.config(bg=self.stop_button_color)

        self.start_button_state = "normal"
        self.start_button_color = "green2"
        self.vent_start_button['state'] = self.start_button_state
        self.vent_start_button.config(bg=self.start_button_color)


        #write settings/state to file
        self.write_config()



    # alarm mute
    def mute_alarm(self):

        #update vars
        self.MUTE += 1

        #write settings/state to file
        self.write_config()


    # assist control on/off
    def assist_control_onoff(self):

        #update vars and GUI
        if self.assist_control == True:
            self.assist_control = False
            self.assist_control_button['text'] = "ASSIST CONTROL OFF"
            self.assist_control_button.config(bg='red')

        else:
            self.assist_control = True
            self.assist_control_button['text'] = "ASSIST CONTROL ON"
            self.assist_control_button.config(bg='green2')

        #write settings/state to file
        self.write_config()


    # write update to file
    def write_config(self):

        #write settings/state to file
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP, 'assist_control':self.assist_control, 'inspiratory_time':self.inspiratory_time,'ItoE_ratio':self.ItoE_ratio}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        baro_dict = {'mode':self.baro_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt}

        with open(os.path.join(self.full_file_dir,'commands_to_stepper.json'), 'w') as f:
            json.dump(stepper_dict, f, indent=4)
        with open(os.path.join(self.full_file_dir,'commands_to_alarm.json'), 'w') as f:
            json.dump(alarm_dict, f, indent=4)
        with open(os.path.join(self.full_file_dir,'commands_to_baro.json'), 'w') as f:
            json.dump(baro_dict, f, indent=4)


    # program close
    def close_program(self):
        #clean instances of support scripts
        for proc in psutil.process_iter():
            if proc.name() == 'python3':
                if len(proc.cmdline()) > 1:
                    script_path = ' '.join(proc.cmdline())
                    if '_control.py' in script_path:
                        print('killing ' + str(script_path))
                        proc.terminate()

        self.master.quit()


# program close, but not inside the class
def close_program2():
    #clean instances of support scripts
    for proc in psutil.process_iter():
        if proc.name() == 'python3':
            if len(proc.cmdline()) > 1:
                script_path = ' '.join(proc.cmdline())
                if '_control.py' in script_path:
                    print('killing ' + str(script_path))
                    proc.terminate()

    root.destroy()


###########################
#MAIN CODE
###########################

print('start main code')

#base stuff needed to make it work
print('define root')
root = tk.Tk()
print('define geometry')
root.geometry("1920x1080")
print('define class')
my_gui = theGUI(root)

root.protocol("WM_DELETE_WINDOW", close_program2)
root.mainloop()

