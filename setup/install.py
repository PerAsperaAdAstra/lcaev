#!/usr/bin/python3

'''
install.py

This program is intended to do as much of the
configuration of the pi as possible to 
operate the LCAEV

Set up for a python3 on a pi 4


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
'''

###########################
#IMPORT MODULES
###########################
import sys
import os




###########################
#MAIN CODE
###########################

#ensure time and locale are set before continuing
usr_in = input('Before running this script, you should set the system locale and time.  Have you done this? [Y/N]: ')

usr_in=str(usr_in)
print(usr_in)

if usr_in != 'y' and usr_in != 'Y' and usr_in != 'yes' and usr_in != 'YES' and usr_in != 'Yes' and usr_in != 'heck yeah':
    print('set the system locale and time, restart the system, and then try again')
    sys.exit()



# begin install
os.system('cd')  #is this required?
os.system('sudo apt-get update -y')
os.system('sudo apt-get upgrade -y')

os.system('sudo apt-get --yes install gedit')

#install adafruit stepper library
os.system('sudo pip3 install adafruit-circuitpython-motorkit')

#install adafruit BMP library
os.system('sudo pip3 install adafruit-BMP')


#update audio for USB devices
os.system('sudo apt-get --yes install mplayer')
os.system('sudo apt-get --yes install pulseaudio')
'''
print('\n\n\n\n\nTo use a USB-only speaker for sound, be sure to change the below fields alsa.conf:')
print('\nFind:')
print('  defaults.ctl.card = 0')
print('  defaults.pcm.card = 0')
print('\nChange to:')
print('  defaults.ctl.card = 1')
print('  defaults.pcm.card = 1')
os.system('sudo gedit /usr/share/alsa/alsa.conf 2>/dev/null')
'''
os.system('sudo mv /usr/share/alsa/alsa.conf /usr/share/alsa/alsa.conf_ORIGINAL')
os.system('sudo cp /home/pi/lcaev/setup/alsa.conf /usr/share/alsa/alsa.conf')

#increase i2c baud
'''
print('\n\n\n\n\nTo increase stepper speed, set i2c baudrate.  Add the below line to the end of the file:\n')
print('dtparam=i2c_baudrate=400000')
os.system('sudo gedit /boot/config.txt 2>/dev/null')
'''
os.system('sudo mv /boot/config.txt /boot/config.txt_ORIGINAL')
os.system('sudo cp /home/pi/lcaev/setup/config.txt /boot/config.txt')

#update rc.local to run from boot
'''
print('\n\n\n\n\nTo start from boot, replace the contents of the lxsession autostart file with the below:\n')
print('@lxpanel --profile LXDE-pi')
print('@pcmanfm --desktop --profile LXDE-pi')
print('@lxterminal --command "/home/pi/lcaev/main/startup.sh"')
print('@xscreensaver -no-splash')
print('@xset s off')
print('@xset -dpms')
os.system('sudo gedit /etc/xdg/lxsession/LXDE-pi/autostart 2>/dev/null')
'''
os.system('sudo mv /etc/xdg/lxsession/LXDE-pi/autostart /etc/xdg/lxsession/LXDE-pi/autostart_ORIGINAL')
os.system('sudo cp /home/pi/lcaev/setup/autostart /etc/xdg/lxsession/LXDE-pi/autostart')

#make scripts executable
os.system('sudo chmod +x ../main/startup.sh ../main/controller_gui.py ../main/stepper_control.py  ../main/alarm_control.py')

print('\n\n\n\n\nNow that install is done, we will enable i2c in raspi-config by doing the below:\n')
print('   select `Ok`')
print('   select `Interfacing Options`')
print('   select `I2C`')
print('   select `Yes`')
print('   select `Ok`')
print('   select `Finish`')

usr_in = input('\n\n\nHit `enter` when ready to continue... ')
os.system('sudo raspi-config')
print('...done!')

print('\n\nSave and reboot the pi!!\n\n')















