#!/usr/bin/python3

'''
data_analysis.py

This progam is intended to combine,
post-process, and plot data gathered
by the debug/developmental data recording
capabilities of the LCAEV

THIS IS NOT INTENDED TO BE RUN ON A PI

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import os
import csv
import sys
import glob
import natsort
import fileinput
from math import *
import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt



###########################
#SUPPORT FUNCTIONS
###########################


def combine_data():

    print('starting data concatenation...')

    #define vars
    system_files = []
    baro_files = []

    for f in glob.glob('data/TEST_SYS_DATA_*'):
        system_files += [f]
    system_files = natsort.natsorted(system_files)

    for f in glob.glob('data/TEST_BARO_DATA_*'):
        baro_files += [f]
    baro_files = natsort.natsorted(baro_files)


    outfilename = 'system_data.csv'
    lines_seen = set() # holds lines already seen
    with open(outfilename,'w') as outfile:
        for fname in system_files:
            with open(fname) as infile:
                for line in infile:
                    if line not in lines_seen: # not a duplicate
                        outfile.write(line)
                        lines_seen.add(line)
    outfile.close()

    outfilename = 'baro_data.csv'
    lines_seen = set() # holds lines already seen

    with open(outfilename,'w') as outfile:
        for fname in baro_files:
            with open(fname) as infile:
                for line in infile:
                    if line not in lines_seen: # not a duplicate
                        outfile.write(line)
                        lines_seen.add(line)
    outfile.close()







def analyze_and_plot():

    print('starting data analysis...')

    #load data
    system_data = defaultdict(list)
    for record in csv.DictReader(open('system_data.csv')):
        for key, val in record.items():    # or iteritems in Python 2
            system_data[key].append(val)

    baro_data = defaultdict(list)
    for record in csv.DictReader(open('baro_data.csv')):
        for key, val in record.items():    # or iteritems in Python 2
            baro_data[key].append(val)



    #create diff'd abs pressure
    Pabs_delta = [0]
    for n in range(1, len(baro_data['pressure'])):
        Pabs_delta+=[float(baro_data['pressure'][n])-float(baro_data['pressure'][n-1])]


    print('plotting...')

    #plot
    n = 0
    n+=1
    plt.figure(n)
    plt.subplot(1, 1, 1)
    plt.plot(baro_data['ontime'],baro_data['pressure'], marker='o')
    plt.title('baro pressure vs time')
    plt.ylabel('pressure (Pa)')
    plt.xlabel('time (s)')

    n+=1
    plt.figure(n)
    plt.subplot(1, 1, 1)
    plt.plot(baro_data['ontime'],Pabs_delta, marker='o')
    plt.title('baro pressure delta ([n]-[n-1]) vs time')
    plt.ylabel('pressure (Pa)')
    plt.xlabel('time (s)')

    n+=1
    plt.figure(n)
    plt.subplot(1, 1, 1)
    plt.plot(baro_data['ontime'],baro_data['delta_pressure'], marker='o')
    plt.title('baro delta pressure vs time')
    plt.ylabel('pressure (Pa)')
    plt.xlabel('time (s)')

    n+=1
    plt.figure(n)
    plt.subplot(1, 1, 1)
    plt.plot(baro_data['ontime'],baro_data['temperature'], marker='o')
    plt.title('Circuit Temp vs time')
    plt.ylabel('Temp (C)')
    plt.xlabel('time (s)')

    n+=1
    plt.figure(n)
    plt.subplot(1, 1, 1)
    plt.plot(system_data['Time'],system_data['CPU temp'], marker='o')
    plt.title('CPU Temp vs time')
    plt.ylabel('Temp (C)')
    plt.xlabel('time (s)')

    n+=1
    plt.figure(n)
    plt.subplot(1, 1, 1)
    plt.plot(system_data['Time'],system_data['RAM Percent'], marker='o')
    plt.title('RAM % vs time')
    plt.ylabel('%')
    plt.xlabel('time (s)')

    n+=1
    plt.figure(n)
    plt.subplot(1, 1, 1)
    plt.plot(system_data['Time'],system_data['CPU Percent'], marker='o')
    plt.title('CPU % vs time')
    plt.ylabel('%')
    plt.xlabel('time (s)')

    plt.show()


###########################
#MAIN CODE
###########################


if __name__ == '__main__':


    
    try:
        start = sys.argv[1]
        print(start)
    except:
        start = None


    if start == 'stitch': combine_data()
    elif start == 'plot': analyze_and_plot()
    else:
        combine_data()
        analyze_and_plot()
    



